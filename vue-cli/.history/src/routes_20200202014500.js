import Home from './components/Home.vue'
import Profile from './components/Profile.vue'
import Login from './components/authentication/LogIn.vue'
import Register from './components/authentication/Register.vue'
import Recommend from './components/recommend/Recommend.vue'
import Search from './components/Search.vue'
import Menus from './components/Menus/Menus.vue'


export const routes = [
    { path: '/', component: Home},
    { path: '/profile', component: Profile},
    { path: '/login', component: Login },
    { path: '/register', component: Register },
    { path: '/recommend', component: Recommend },
    { path: '/search', component: Search },
    { path: '/menus', component: Menus },
];