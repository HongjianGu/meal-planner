import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const = new Vuex.Store({
    state: {
        building: ''
    }
});