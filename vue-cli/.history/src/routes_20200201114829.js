import Home from './components/Home.vue'
import Setting from './components/Setting.vue'
import Login from './components/authentication/LogIn.vue'
import Register from './components/authentication/Register.vue'
import Recommend from './components/recommand/Recommend.vue'
import Search from './components/recommand/Recommend.vue'


export const routes = [
    { path: '/', component: Home},
    { path: '/setting', component: Setting},
    { path: '/login', component: Login },
    { path: '/register', component: Register },
    { path: '/recommend', component: Recommend },
    { path: '/recommend', component: Recommend },
];