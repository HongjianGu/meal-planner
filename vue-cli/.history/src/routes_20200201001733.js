import Home from './components/Home.vue'
import Setting from './components/Setting.vue'
import Login from './components/authentication/LogIn.vue'
import Register from './components/authentication/Register.vue'
import Recommand from './components/recommand/Recommand.vue'

export const routes = [
    { path: '/', component: Home},
    { path: '/setting', component: Setting},
    { path: '/login', component: Login },
    { path: '/register', component: Register },
    { path: '/recommand', component: Recommand }

];