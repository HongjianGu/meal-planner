# McTeam

A meal planning website using Vue, Firebase, and the Google Maps API to help students plan their McGill meal plans.

Built and submitted at McHacks 7:
https://devpost.com/software/meal-planner-6zvgmd
